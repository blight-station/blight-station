/decl/cultural_info/culture/adherent
	name = CULTURE_ADHERENT
	description = "The Vigil is a relatively loose association of machine-servitors, adherents, built by a now-extinct culture. \
	They are devoted to the memory of their long-dead creators, destroyed by the Scream, a solar flare which wiped out the vast \
	majority of records of the creators and scrambled many sensor systems and minds, leaving the surviving adherents confused \
	and disoriented for hundreds of years following. Now in contact with humanity, the Vigil is tentatively making inroads on \
	a place in the wider galactic culture."
	hidden_from_codex = TRUE
	language = LANGUAGE_ADHERENT
	secondary_langs = list(
//Human
LANGUAGE_HUMAN_EURO,     // "Zurich Accord Common"
LANGUAGE_HUMAN_CHINESE,  // "Yangyu"
LANGUAGE_HUMAN_ARABIC, // "Prototype Standard Arabic"
LANGUAGE_HUMAN_INDIAN,   // "New Dehlavi"
LANGUAGE_HUMAN_IBERIAN,  // "Iberian"
LANGUAGE_HUMAN_RUSSIAN,  // "Pan-Slavic"
LANGUAGE_HUMAN_SELENIAN, // "Selenian"

//Human misc
LANGUAGE_GUTTER,        // "Gutter"
LANGUAGE_LEGALESE,      // "Legalese"
LANGUAGE_SPACER,        // "Spacer"

//Alien
LANGUAGE_EAL,             //  "Encoded Audio Language"
LANGUAGE_UNATHI_SINTA,    // "Sinta'unathi"
LANGUAGE_UNATHI_YEOSA,    //  "Yeosa'unathi"
LANGUAGE_SKRELLIAN,       //  "Skrellian"
LANGUAGE_ROOTLOCAL,       //  "Local Rootspeak"
LANGUAGE_ROOTGLOBAL,      //  "Global Rootspeak"
LANGUAGE_ADHERENT,        //  "Protocol"
LANGUAGE_VOX,             //  "Vox-pidgin"
LANGUAGE_NABBER,          //  "Serpentid"
LANGUAGE_TESHARI,		//	"Schechi"
LANGUAGE_CANILUNZT,        //  "Canilunzt"
//Antag
//LANGUAGE_CULT             // "Cult"
//LANGUAGE_CULT_GLOBAL      // "Occult"
//LANGUAGE_ALIUM           //  "Alium"

//Other
//LANGUAGE_PRIMITIVE        // "Primitive"
LANGUAGE_SIGN,             // "Sign Language"
//LANGUAGE_ROBOT_GLOBAL     // "Robot Talk"
//LANGUAGE_DRONE_GLOBAL     // "Drone Talk"
//LANGUAGE_CHANGELING_GLOBAL// "Changeling"
//LANGUAGE_BORER_GLOBAL     // "Cortical Link"
LANGUAGE_MANTID_NONVOCAL,  // "Ascent-Glow"
LANGUAGE_MANTID_VOCAL     // "Ascent-Voc"
//LANGUAGE_MANTID_BROADCAST // "Worldnet"
//LANGUAGE_ZOMBIE
	)

/decl/cultural_info/culture/adherent/get_random_name(var/gender)
	return "[uppertext("[pick(GLOB.full_alphabet)][pick(GLOB.full_alphabet)]-[pick(GLOB.full_alphabet)] [rand(1000,9999)]")]"

/decl/cultural_info/culture/adherent/sanitize_name(name)
	return sanitizeName(name, allow_numbers=TRUE)

/decl/cultural_info/culture/diona
	name = CULTURE_DIONA
	description = "To exist as a diona gestalt is to be a throng of tiny sparks forming a pyre, singing out into \
	the dark, endlessly curious and seeking new knowledge to return to the chorus that spawned 'you'."
	language = LANGUAGE_ROOTLOCAL
	additional_langs = list(LANGUAGE_ROOTGLOBAL)
	secondary_langs = list(
//Human
LANGUAGE_HUMAN_EURO,     // "Zurich Accord Common"
LANGUAGE_HUMAN_CHINESE,  // "Yangyu"
LANGUAGE_HUMAN_ARABIC, // "Prototype Standard Arabic"
LANGUAGE_HUMAN_INDIAN,   // "New Dehlavi"
LANGUAGE_HUMAN_IBERIAN,  // "Iberian"
LANGUAGE_HUMAN_RUSSIAN,  // "Pan-Slavic"
LANGUAGE_HUMAN_SELENIAN, // "Selenian"

//Human misc
LANGUAGE_GUTTER,        // "Gutter"
LANGUAGE_LEGALESE,      // "Legalese"
LANGUAGE_SPACER,        // "Spacer"

//Alien
LANGUAGE_EAL,             //  "Encoded Audio Language"
LANGUAGE_UNATHI_SINTA,    // "Sinta'unathi"
LANGUAGE_UNATHI_YEOSA,    //  "Yeosa'unathi"
LANGUAGE_SKRELLIAN,       //  "Skrellian"
LANGUAGE_ROOTLOCAL,       //  "Local Rootspeak"
LANGUAGE_ROOTGLOBAL,      //  "Global Rootspeak"
LANGUAGE_ADHERENT,        //  "Protocol"
LANGUAGE_VOX,             //  "Vox-pidgin"
LANGUAGE_NABBER,          //  "Serpentid"
LANGUAGE_TESHARI,		//	"Schechi"
LANGUAGE_CANILUNZT,        //  "Canilunzt"
//Antag
//LANGUAGE_CULT             // "Cult"
//LANGUAGE_CULT_GLOBAL      // "Occult"
//LANGUAGE_ALIUM           //  "Alium"

//Other
//LANGUAGE_PRIMITIVE        // "Primitive"
LANGUAGE_SIGN,             // "Sign Language"
//LANGUAGE_ROBOT_GLOBAL     // "Robot Talk"
//LANGUAGE_DRONE_GLOBAL     // "Drone Talk"
//LANGUAGE_CHANGELING_GLOBAL// "Changeling"
//LANGUAGE_BORER_GLOBAL     // "Cortical Link"
LANGUAGE_MANTID_NONVOCAL,  // "Ascent-Glow"
LANGUAGE_MANTID_VOCAL     // "Ascent-Voc"
//LANGUAGE_MANTID_BROADCAST // "Worldnet"
//LANGUAGE_ZOMBIE           // "Zombie"
	)

#!/bin/bash

# Default maps which are already included in the dme
# Other exempt maps also go in here. Please include a comment describing why it's exempt if it's not a default map

#EXEMPT_MAPS=(
#)

# Include everything but backup files and nightmare inserts
for filename in $(find maps -name '*.dmm' ! -wholename '*/backup/*.dmm' ! -wholename '*/Nightmare/*.dmm'); do
    # Skip maps that are already included/exempt from CI
    if [[ ${EXEMPT_MAPS[*]} =~ "$filename" ]]; then
        continue
    fi

    # Insert the includes
    echo "#include \"$filename\"" >> ColonialMarinesALPHA.dme
done
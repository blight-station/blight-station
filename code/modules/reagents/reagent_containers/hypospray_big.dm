////////////////////////////
/// Large Autoinjectors ///
///////////////////////////

// These have a 15u capacity, somewhat higher tech level, and generally more useful chems, but are otherwise the same as the regular autoinjectors.
/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector
	name = "empty hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity."
	icon_state = "big_injector"
	amount_per_transfer_from_this = 15
	band_color = COLOR_CYAN
	volume = 15
	origin_tech = list(TECH_BIO = 4)
	starts_with = list(/datum/reagent/inaprovaline = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/brute
	name = "trauma hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This one is made to be used on victims of \
	moderate blunt trauma."
	band_color = COLOR_RED
	starts_with = list(/datum/reagent/bicaridine = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/burn
	name = "burn hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This one is made to be used on burn victims, \
	featuring an optimized chemical mixture to allow for rapid healing."
	band_color = COLOR_ORANGE
	starts_with = list(/datum/reagent/kelotane = 7.5, /datum/reagent/dermaline = 7.5)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/toxin
	name = "toxin hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This one is made to counteract toxins."
	band_color = COLOR_GREEN
	starts_with = list(/datum/reagent/dylovene = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/oxy
	name = "oxy hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This one is made to counteract oxygen \
	deprivation."
	band_color = COLOR_BLUE
	starts_with = list(/datum/reagent/dexalinp = 10, /datum/reagent/tricordrazine = 5)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/purity
	name = "purity hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This variant excels at \
	resolving viruses, infections, radiation, and genetic maladies."
	band_color = COLOR_GREEN
	starts_with = list(/datum/reagent/spaceacillin = 9, /datum/reagent/arithrazine = 5, /datum/reagent/ryetalyn = 1)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/pain
	name = "pain hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This one contains potent painkillers."
	band_color = COLOR_PURPLE
	starts_with = list(/datum/reagent/tramadol = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/organ
	name = "organ hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  Organ damage is resolved by this variant."
	band_color = COLOR_PURPLE
	starts_with = list(/datum/reagent/alkysine = 3, /datum/reagent/imidazoline = 2, /datum/reagent/peridaxon = 10)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/combat
	name = "combat hypo"
	desc = "A refined version of the standard autoinjector, allowing greater capacity.  This is a more dangerous and potentially \
	addictive hypo compared to others, as it contains a potent cocktail of various chemicals to optimize the recipient's combat \
	ability."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/bicaridine = 3, /datum/reagent/kelotane = 1.5, /datum/reagent/dermaline = 1.5, /datum/reagent/tramadol/oxycodone = 3, /datum/reagent/hyperzine = 3, /datum/reagent/tricordrazine = 3)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/clotting
	name = "clotting agent"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This variant excels at treating bleeding wounds and internal bleeding."
	band_color = COLOR_PURPLE
	starts_with = list(/datum/reagent/inaprovaline = 5, /datum/reagent/myelamine = 10)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/bonemed
	name = "bone repair injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one excels at treating damage to bones."
	band_color = COLOR_PINK
	starts_with = list(/datum/reagent/inaprovaline = 5, /datum/reagent/osteodaxon = 10)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/glucose
	name = "glucose hypo"
	desc = "A hypoinjector filled with glucose, used for critically malnourished patients and voidsuited workers."
	band_color = COLOR_BROWN
	starts_with = list(/datum/reagent/nutriment/glucose = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/stimm
	name = "stimm injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one is filled with a home-made stimulant, with some serious side-effects."
	band_color = null
	starts_with = list(/datum/reagent/toxin/stimm = 10) // More than 10u will OD.

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/expired
	name = "expired injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one has had its contents expire a long time ago, using it now will probably make someone sick, or worse."
	band_color = null
	starts_with = list(/datum/reagent/toxin/expired_medicine = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/soporific
	name = "soporific injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one is sometimes used by orderlies, as it has soporifics, which make someone tired and fall asleep."
	band_color = COLOR_CYAN
	starts_with = list(/datum/reagent/soporific = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/cyanide
	name = "cyanide injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one contains cyanide, a lethal poison. It being inside a medical autoinjector has certain unsettling implications."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/toxin/cyanide = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/serotrotium
	name = "serotrotium injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one is filled with serotrotium, which causes concentrated production of the serotonin neurotransmitter in humans."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/serotrotium = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/space_drugs
	name = "illicit injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one contains various illicit drugs, held inside a hypospray to make smuggling easier."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/space_drugs = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/cryptobiolin
	name = "cryptobiolin injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one contains cryptobiolin, which causes confusion."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/cryptobiolin = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/impedrezene
	name = "impedrezene injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one has impedrezene inside, a narcotic that impairs higher brain functioning. \
	This autoinjector is almost certainly created illegitimately."
	starts_with = list(/datum/reagent/impedrezene = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/mindbreaker
	name = "mindbreaker injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This one stores the dangerous hallucinogen called 'Mindbreaker', likely put in place \
	by illicit groups hoping to hide their product."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/mindbreaker = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/psilocybin
	name = "psilocybin injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This has psilocybin inside, which is a strong psychotropic derived from certain species of mushroom. \
	This autoinjector likely was made by criminal elements to avoid detection from casual inspection."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/psilocybin = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/mutagen
	name = "unstable mutagen injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This contains unstable mutagen, which makes using this a very bad idea. It will either \
	ruin your genetic health, turn you into a Five Points violation, or both!"
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/mutagen = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/lexorin
	name = "lexorin injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	This contains lexorin, a dangerous toxin that stops respiration, and has been \
	implicated in several high-profile assassinations in the past."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/lexorin = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/healing_nanites
	name = "medical nanite injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	The injector stores a slurry of highly advanced and specialized nanomachines designed \
	to restore bodily health from within. The nanomachines are short-lived but degrade \
	harmlessly, and cannot self-replicate in order to remain Five Points compliant."
	band_color = COLOR_BLACK
	starts_with = list(/datum/reagent/healing_nanites = 15)

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/defective_nanites
	name = "defective nanite injector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. \
	The injector stores a slurry of highly advanced and specialized nanomachines that \
	are unfortunately malfunctioning, making them unsafe to use inside of a living body. \
	Because of the Five Points, these nanites cannot self-replicate."
	band_color = COLOR_BLACK
	starts_with = list(.datum/reagent/defective_nanites = 15)

// Here are the paths for all hypos that start unidentified.
// Usually you want to use a random spawner instead of using them directly, unless you're spawning these live for adminbus purposes.
// Also good for scavngeable loot and for Darwinning the crew.

// The good.
/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/brute/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/burn/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/toxin/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/oxy/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/purity/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/pain/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/organ/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/clotting/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/bonemed/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/combat/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/healing_nanites/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

// The somewhat bad.
/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/stimm/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/space_drugs/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/expired/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/serotrotium/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/cryptobiolin/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/mindbreaker/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/psilocybin/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/soporific/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

// The very bad.
/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/cyanide/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/impedrezene/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/mutagen/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/defective_nanites/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK

/obj/item/weapon/reagent_containers/hypospray/autoinjector/biginjector/contaminated/unidentified
	name = "unidentified autoinjector"
	desc = "A refined version of the standard autoinjector, allowing greater capacity. This one has lost any identifying marks so it is unknown what it contains."
	band_color = COLOR_BLACK